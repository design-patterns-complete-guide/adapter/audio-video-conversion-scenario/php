<?php

//Target interface
interface MediaPlayer
{
    public function play();
}

//Adaptee classes
class MP3Player
{
    public function PlayMP3()
    {
        echo "Playing MP3 Audio\n";
    }
}

class MP4Player
{
    public function playMP4()
    {
        echo "Playing MP4 Video\n";
    }
}

//Adapter Classes
class MP3Adapter implements MediaPlayer
{
    private $mp3Player;

    public function __construct(MP3Player $mp3Player)
    {
        $this->mp3Player = $mp3Player;
    }

    public function play()
    {
        $this->mp3Player->PlayMP3();
    }
}

class MP4Adapter implements MediaPlayer
{
    private $mp4Player;

    public function __construct(MP4Player $mp4Player)
    {
        $this->mp4Player = $mp4Player;
    }

    public function play()
    {
        $this->mp4Player->playMP4();
    }
}

//client code
$mp4Player = new MP4Player();
$mp4Adapter = new MP4Adapter($mp4Player);
$mp4Adapter->play();

$mp3Player = new MP3Player();
$mp3Adapter = new MP3Adapter($mp3Player);
$mp3Adapter->play();

